package lg

import "context"

type Logger interface {
	Error(msg string)
	Tracef(msg string, args ...any)
	Debugf(msg string, args ...any)
	Infof(msg string, args ...any)
	Warnf(msg string, args ...any)
	Errorf(msg string, args ...any)
	Fatalf(msg string, args ...any)
	Panicf(msg string, args ...any)
}

type CtxLogger interface {
	Error(err error)
	DefError(err *error)
	DError(msg string)
	Tracef(msg string, args ...any)
	Debugf(msg string, args ...any)
	Infof(msg string, args ...any)
	Warnf(msg string, args ...any)
	Errorf(msg string, args ...any)
	Fatalf(msg string, args ...any)
	Panicf(msg string, args ...any)
	AddTags(tag ...Tag) CtxLogger
	Ctx() context.Context
	context.Context
	Send()
	SpanLog(top33ic, format string, args ...any)
}
