module gitlab.com/8207042/logger

go 1.21.1

require (
	github.com/google/uuid v1.4.0
	github.com/mattn/go-colorable v0.1.13
	github.com/opentracing/opentracing-go v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/rogpeppe/go-internal v1.11.0
)

require (
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/mod v0.13.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
