package lg

import (
	"context"
	tracing "github.com/opentracing/opentracing-go"
	"gitlab.com/8207042/logger/internal/zerolog"
	"net/http"
	"sync"
)

type Tag string

const (
	Sql            = Tag("sql")
	General        = Tag("general")
	Service        = Tag("service")
	Warning        = Tag("warning")
	OrdersOffers   = Tag("ordersOffers")
	Balance        = Tag("balance")
	FatalPanic     = Tag("fatalPanic")
	IncomeWithdraw = Tag("incomeWithdraw")
)

type lg struct {
	zl      *zerolog.Logger
	logs    *logsChan
	wgFP    sync.WaitGroup
	wgLogs  sync.WaitGroup
	wgFlush sync.WaitGroup
	mu      sync.Mutex
	stopCh  chan struct{}
}

type logsChan struct {
	logs chan []byte
}

func (lc *logsChan) Write(p []byte) (n int, err error) {
	cp := make([]byte, len(p))
	copy(cp, p)

	lc.logs <- cp
	return len(cp), nil
}

type lgCtx struct {
	empty  bool
	zl     *zerolog.Logger
	logs   *logsList
	mu     sync.Mutex
	header http.Header
	span   tracing.Span
	wg     sync.WaitGroup
	wgFP   sync.WaitGroup
	tags   map[string]struct{}
	context.Context
}

type logsList struct {
	logs [][]byte
}

func (ll *logsList) Write(p []byte) (n int, err error) {
	cp := make([]byte, len(p))
	copy(cp, p)

	ll.logs = append(ll.logs, cp)
	return len(p), nil
}
